<?php

/**
 * The default bitgravity_live settings page.
 */
function bitgravity_live_settings_page($op = NULL) {
  $output = drupal_get_form('bitgravity_live_settings_form');
  return $output;
}

/**
 * The default bitgravity_live sources page.
 */
function bitgravity_live_sources_page($op = NULL) {
  $output = bitgravity_live_admin_source_list();
  return $output;
}


/**
 * This form will be displayed both at /admin/settings/media_bitgravity and
 * admin/content/emfield.
 */
function bitgravity_live_settings_form() {  
  $form = array();
  
  //build the video sources options
  $source_options = array();
  $source_options_array = _bitgravity_live_get_sources();
  foreach($source_options_array as $source_option) {
    $source_options[$source_option['sid']] = $source_option['label']." - ".$source_option['file'];
  }
  
  $form['bitgravity_live_video_sources'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Video Sources'),
    '#description' => t('Select video sources you would like to make available to the player. '.l('Add More Sources', 'admin/settings/bitgravity_live/sources/add')),
    '#options' => $source_options,
    '#default_value' => variable_get('bitgravity_live_video_sources', ''),
  );
  
  //set up the default options from the available sources (this is different from above because the value that should be used is the file name for the file instead of it's sid)
  $default_source_options = array();
  foreach($source_options_array as $source_option) {
    $default_source_options[$source_option['file']] = $source_option['label']." - ".$source_option['file'];
  }
  $form['bitgravity_live_default_file'] = array(
    '#type' => 'radios',
    '#title' => t('Default Source'),
    '#description' => t('Enter the URL of the video source file you want to be the default to begin playback. Will Default to the first one if you do not enter one here.'),
    '#options' => $default_source_options,
    '#default_value' => variable_get('bitgravity_live_default_file', ''),
  );
  
  $form['bitgravity_live_playback_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Playback Options'),
    '#description' => t(''),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['bitgravity_live_playback_options']['bitgravity_live_auto_play'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto Play'),
    '#description' => t('Begin playback automatically. If not checked, the video will not begin until the user clicks the play button.'),
    '#default_value' => variable_get('bitgravity_live_auto_play', 0),
  );
  $form['bitgravity_live_playback_options']['bitgravity_live_buffer_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Buffer Time'),
    '#description' => t('When video buffer empties, at least this many seconds of video will load before the player continues playing. Increase this number for fewer (though longer) download waits.'),
    '#default_value' => variable_get('bitgravity_live_buffer_time', '1.5'),
    '#prefix' => 'Preload (buffer) up to ',
    '#suffix' => 'seconds of video.',
  );
  $form['bitgravity_live_playback_options']['bitgravity_live_scrub_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Scrub Mode'),
    '#description' => t('Advanced mode makes use of BitGravity\'s Advanced Progressive™ technology to allow instant scrubbing to any point in a video by making advanced requests to the server. FLV video requires pre-processing with FLVMDU. <strong>Note: Without advanced mode set, bit rate adjustments will restart the video.</strong>'),
    '#options' => array(t('simple'), t('advanced')),
    '#default_value' => variable_get('bitgravity_live_scrub_mode', 0),
  );
  $form['bitgravity_live_playback_options']['bitgravity_live_cross_fading'] = array(
    '#type' => 'checkbox',
    '#title' => t('Cross Fading'),
    '#description' => t('Enabling cross fading will crossfade video when switching bit rates. <strong>Not recommended for HD video, or older computers.</strong>'),
    '#default_value' => variable_get('bitgravity_live_cross_fading', 0),
  );
  $form['bitgravity_live_playback_options']['bitgravity_live_auto_bit_rate'] = array(
    '#type' => 'radios',
    '#title' => t('Auto Bit Rate'),
    '#description' => t('Player will drop or jump to streams based on periodic connection measurements. Unless set to "disabled" this is toggleable by the user during runtime, and defaults to the value of this parameter.'),
    '#options' => array(t("on"), t("off"), t("disabled")),
    '#default_value' => variable_get('bitgravity_live_auto_bit_rate', 2),
  );
  $form['bitgravity_live_playback_options']['bitgravity_live_allow_gpu_scaling'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow GPU Scaling'),
    '#description' => t('If system capabilities allow for hardware scaling of video, this method will be used when entering full screen mode if this parameter is set to true.'),
    '#default_value' => variable_get('bitgravity_live_allow_gpu_scaling', 0),
  );
  $form['bitgravity_live_playback_options']['bitgravity_live_force_reconnect'] = array(
    '#type' => 'textfield',
    '#title' => t('Force Reconnect'),
    '#description' => t('Some internet browsers have difficulty storing a very long contiguous video file in memory, so setting this parameter to a number of seconds will force a reconnect to the stream every X number of seconds. This can be set to 0 to disable the function.'),
    '#default_value' => variable_get('bitgravity_live_force_reconnect', 1800),
  );
  $form['bitgravity_live_playback_options']['bitgravity_live_audio_channel'] = array(
    '#type' => 'radios',
    '#title' => t('Audio Channel'),
    '#description' => t('The specified channel will be duplicated to both speakers and the opposite channel will be muted. e.g. setting this to "left" will cause the left audio channel to be played in both speakers, and the right channel to be muted.'),
    '#options' => array(t("both"), t("left"), t("right")),
    '#default_value' => variable_get('bitgravity_live_audio_channel', 0),
  );
  
  $form['bitgravity_live_player_configuration'] = array(
    '#type' => 'fieldset',
    '#title' => t('Player Configuration'),
    '#description' => t('Player height should allow 20 for control bar'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['bitgravity_live_player_configuration']['bitgravity_live_player_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#description' => t('The width of the player.'),
    '#size' => 7,
    '#maxlength' => 255,
    '#default_value' => variable_get('bitgravity_live_player_width', 0),
  );
  $form['bitgravity_live_player_configuration']['bitgravity_live_player_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#description' => t('Allow 20 extra pixels for the control bar.'),
    '#size' => 7,
    '#maxlength' => 255,
    '#default_value' => variable_get('bitgravity_live_player_height', 0),
  );
  $form['bitgravity_live_player_configuration']['bitgravity_live_force_ratio'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use my video\'s built in aspect ratio'),
    '#default_value' => variable_get('bitgravity_live_force_ratio', 1),
  );
  $form['bitgravity_live_player_configuration']['bitgravity_live_dimensions'] = array(
    '#type' => 'select',
    '#title' => t('Dimensions'),
    '#description' => t('Specify a fixed aspect ratio if you are not using the default aspect ratio.'),
    '#options' => array("16/9", "4/3"),
    '#default_value' => variable_get('bitgravity_live_dimensions', "16/9"),
  );
  $form['bitgravity_live_player_configuration']['bitgravity_live_default_scale_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Default Scale Mode'),
    '#description' => t('If the video dimensions differ from that of the player, the video will be stretched according to this mode. "automatic" will proportionally stretch the video as large as is possible without hiding any part of the video. "stretch" will stretch the video to fill the player. "fill" will proportionally stretch the video to just large enough to fill the player. i.e. video may be cropped. Viewers may switch between these during playback. Choose the default below.'),
    '#options' => array(t("automatic"), t("stretch"), t("fill")),
    '#default_value' => variable_get('bitgravity_live_default_scale_mode', 0),
  );
  $form['bitgravity_live_player_configuration']['bitgravity_live_volume'] = array(
    '#type' => 'select',
    '#title' => t('Volume'),
    '#multiple' => TRUE,
    '#description' => t('A number between 0 and 1, with 0 representing mute and 1 representing full volume. This will set the initial volume, but the user will be able to adjust it in the player.'),
    '#options' => array("0", "0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1"),
    '#default_value' => variable_get('bitgravity_live_volume', 10),
  );
  $form['bitgravity_live_player_configuration']['bitgravity_live_background'] = array(
    '#type' => 'textfield',
    '#title' => t('Background'),
    '#description' => t('The URL of the image displayed on behind the video. Mainly visible only when the video is of unequal aspect ratio to the player.'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => variable_get('bitgravity_live_background', ''),
  );
  $form['bitgravity_live_player_configuration']['bitgravity_live_disable_live_controls'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable Live Controls'),
    '#default_value' => variable_get('bitgravity_live_disable_live_controls', 0),
    '#description' => t('Setting this parameter to true will hide all controls (except playhead time) when viewing a BitGravity LiveBroadcast stream.'),
  );
  $form['bitgravity_live_player_configuration']['bitgravity_live_version_warnings'] = array(
    '#type' => 'checkbox',
    '#title' => t('Version Warnings'),
    '#default_value' => variable_get('bitgravity_live_version_warnings', 0),
    '#description' => t('Certain video features, such as h.264 support, are only available for sufficiently recent versions of Adobe Flash Player. The player will alert the end user to any version shortcomings. Set this parameter to false if you would like these warnings to be disabled.'),
  );
  $form['bitgravity_live_player_configuration']['bitgravity_live_no_text'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide Message Text'),
    '#default_value' => variable_get('bitgravity_live_no_text', 0),
    '#description' => t('Messages such as "Buffering", "Paused", etc. can be hidden by setting this parameter to true. The buffering / connecting animation will continue to appear.'),
  );
  
  $form['bitgravity_live_player_theme'] = array(
    '#type' => 'fieldset',
    '#title' => t('Player Theme'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['bitgravity_live_player_theme']['bitgravity_live_color_base'] = array(
    '#type' => 'colorpicker_textfield',
    //'#type' => 'textfield',
    '#title' => t('Control Bar'),
    '#size' => 7,
    '#maxlength' => 255,
    '#default_value' => variable_get('bitgravity_live_color_base', ''),
  );
  $form['bitgravity_live_player_theme']['bitgravity_live_color_control'] = array(
    '#type' => 'colorpicker_textfield',
    //'#type' => 'textfield',
    '#title' => t('Color of Controls'),
    '#size' => 7,
    '#maxlength' => 255,
    '#default_value' => variable_get('bitgravity_live_color_control', ''),
  );
  $form['bitgravity_live_player_theme']['bitgravity_live_color_highlight'] = array(
    '#type' => 'colorpicker_textfield',
    //'#type' => 'textfield',
    '#title' => t('Color of Controls on Mouse-over'),
    '#size' => 7,
    '#maxlength' => 255,
    '#default_value' => variable_get('bitgravity_live_color_highlight', ''),
  );
  $form['bitgravity_live_player_theme']['bitgravity_live_color_feature'] = array(
    '#type' => 'colorpicker_textfield',
    //'#type' => 'textfield',
    '#title' => t('Color of Scrubber & Other Features'),
    '#size' => 7,
    '#maxlength' => 255,
    '#default_value' => variable_get('bitgravity_live_color_feature', ''),
  );
  $form['bitgravity_live_player_theme']['bitgravity_live_logo_image'] = array(
    '#type' => 'textfield',
    '#title' => t('Logo Image'),
    '#description' => t('The URL of the image displayed one of the player\'s corners.'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => variable_get('bitgravity_live_logo_image', ''),
  );
  $form['bitgravity_live_player_theme']['bitgravity_live_logo_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Logo Link'),
    '#description' => t('Clicking the logo will take you to this URL.'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => variable_get('bitgravity_live_logo_link', ''),
  );
  $form['bitgravity_live_player_theme']['bitgravity_live_logo_position'] = array(
    '#type' => 'select',
    '#title' => t('Logo Position'),
    '#multiple' => FALSE,
    '#description' => t('The position of the logo in the video window.'),
    '#options' => array("none" => "", "Top Left"=>"topleft", "Top Right"=>"topright", "Bottom Left"=>"bottomleft", "Bottom Right"=>"bottomright"),
    '#default_value' => variable_get('bitgravity_live_logo_position', ''),
  );
  $form['bitgravity_live_player_theme']['bitgravity_live_thumbnail'] = array(
    '#type' => 'textfield',
    '#title' => t('Thumbnail'),
    '#description' => t('The URL of the image displayed until the video starts playing.'),
  );
  $form['bitgravity_live_player_theme']['bitgravity_live_replay_thumbnail'] = array(
    '#type' => 'textfield',
    '#title' => t('Replay Thumbnail'),
    '#description' => t('One of [on|off|url]. "on" will display the same thumbnail behind the replay button as behind the play button. Entering "off" will display no image behind the replay button. If you know the URL of the image you want to use, enter it in the field.'),
    '#default_value' => variable_get('bitgravity_live_replay_thumbnail', 'on'),
  );
  //TODO: Check the current value of Auto Play and indicate it here in the description.
  $form['bitgravity_live_player_theme']['bitgravity_live_transparent_play_button'] = array(
    '#type' => 'checkbox',
    '#title' => t('Transparent Play Button'),
    '#description' => t('When AutoPlay is disabled, setting this to true will cause the full area of the player to assume the role of the large circular centered play button (which will itself be hidden). Set the Thumbnail parameter to display an image containing a play button or some other indication to click the image to start the video.'),
    '#default_value' => variable_get('bitgravity_live_transparent_play_button', 0),
  );
  $form['bitgravity_live_player_theme']['bitgravity_live_transparent_replay_button'] = array(
    '#type' => 'checkbox',
    '#title' => t('Transparent Replay Button'),
    '#description' => t('Setting this to true will cause the full area of the player to assume the role of the large circular centered replay button (which will itself be hidden). Set the Replay Thumbnail parameter to display an image containing a replay button or similar indication.'),
    '#default_value' => variable_get('bitgravity_live_transparent_replay_button', 0),
  );
  
  $form['bitgravity_live_advertising'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advertising'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['bitgravity_live_advertising']['bitgravity_live_preroll'] = array(
    '#type' => 'textfield',
    '#title' => t('Preroll'),
    '#description' => t('The URL of the video file played before the main video. Playback controls are hidden.'),
    '#default_value' => variable_get('bitgravity_live_preroll', ''),
  );
  $form['bitgravity_live_advertising']['bitgravity_live_postroll'] = array(
    '#type' => 'textfield',
    '#title' => t('Postroll'),
    '#description' => t('The URL of the video file played after the main video. Playback controls are hidden.'),
    '#default_value' => variable_get('bitgravity_live_postroll', ''),
  );
  
  $form['bitgravity_live_development_testing'] = array(
    '#type' => 'fieldset',
    '#title' => t('Development & Testing'),
    '#description' => t('These tools are meant for development and testing purposes only. Do not use on Production sites.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['bitgravity_live_development_testing']['bitgravity_live_allow_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow Debug'),
    '#description' => t('When focus is on the player, pressing "d" will bring up a debug overlay. To disable this functionality, set AllowDebug to false.'),
    '#default_value' => variable_get('bitgravity_live_allow_debug', 0),
  );
  $form['bitgravity_live_development_testing']['bitgravity_live_open_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Open Debug on Start'),
    '#description' => t('Opens the debug window by default on player start.'),
    '#default_value' => variable_get('bitgravity_live_open_debug', 0),
  );
  $form['bitgravity_live_development_testing']['bitgravity_live_allow_info'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow Info'),
    '#description' => t('When focus is on the player, pressing the "i" key will bring up some information on the player state. To disable this functionality, set Allow Info to false.'),
    '#default_value' => variable_get('bitgravity_live_allow_info', 1),
  );
  
  return system_settings_form($form);
}


/**
 * Call back function to lists all sources that have been added with a link to delete them
 */
function bitgravity_live_admin_source_list() {
  //get the sources
  $sql = "SELECT file, bitrate, label, sid FROM {bitgravity_live_sources}";
  $result = db_query($sql);
  while ($row = db_fetch_object($result)) {
    $delete_link = l('Delete', 'admin/settings/bitgravity_live/sources/delete/'.$row->sid);
    $rows[] = array($row->file, $row->bitrate, $row->label,$delete_link);
  }
  if(count($rows)>0) {
    //set the header row for the table
    $header = array('File (URL)','Bit Rate','Label','Action');
    //return a table of the sources
    return theme('table',$header,$rows);
  }
  else {
    return 'No Sources available at this time. '.l('Add more sources', 'admin/settings/bitgravity_live/sources/add');
  }
}

/**
 * Delete callback function to delete a source
 * @param $sid
 *  the source id, must be numeric
 * @return 
 *   a drupal message
 */
function bitgravity_live_admin_source_delete($sid) {
  if(is_numeric($sid)) {
    $sql = "DELETE FROM {bitgravity_live_sources} WHERE sid = %d";
    if(db_query($sql, $sid)){
      drupal_set_message(t('Source has been deleted.'));
    }
    else {
      drupal_set_message(t('Source cannot be deleted.'),'error');
    }
  }
  else {
    drupal_set_message(t('No source ID given. You may have reached this page accidentally.'),'error');
  }
  drupal_goto('admin/settings/bitgravity_live/sources/list');
}

/**
 * Provide an admin form to add new source source to the list of available 
 * sources to set as a default source on the main admin settings form
 */
function bitgravity_live_admin_source_form() {
  $form = array();
  
  $form['bitgravity_live_default_file'] = array(
    '#type' => 'textfield',
    '#title' => t('File'),
    '#description' => t('The URL of the video file you wish to play. If you have multiple streams the highest quality bitrate stream should be the first one you enter.'),
    '#default_value' => variable_get('bitgravity_live_default_file', ''),
    '#required' => TRUE,
  );
  $form['bitgravity_live_file_label'] = array(
    '#type' => 'textfield',
    '#title' => t('File Label'),
    '#description' => t('When connecting to multiple streams, a label should be set for each to appear in the player\'s bit rate menu. Example: High Banwidth'),
    '#default_value' => variable_get('bitgravity_live_file_label', ''),
    '#required' => TRUE,
  );
  $form['bitgravity_live_file_bit_rate'] = array(
    '#type' => 'textfield',
    '#title' => t('File Bit Rate'),
    '#description' => t('When connecting to multiple streams, a bit rate in Kbps should be set for each. Example: 400'),
    '#default_value' => variable_get('bitgravity_live_file_bit_rate', ''),
  );
  
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  
  
  return $form;
}

/**
 * Validations handler for the add source form
 */
function bitgravity_live_admin_source_form_validate($form, &$form_state) {
  if(!is_numeric($form_state['values']['bitgravity_live_file_bit_rate'])) {
    form_set_error('bitgravity_live_file_bit_rate', t('The Bit Rate must be a number. (Example: 400 or 1000)'));
  }
  if(!valid_url($form_state['values']['bitgravity_live_default_file'], TRUE)) {
    form_set_error('bitgravity_live_default_file', t('The File must be a valid URL (http://bglive-a.bitgravity.com/youraccount/live/feed01). You must include the http: part!'));
  }
  
}

/**
 * Submission handler for the add source form
 */
function bitgravity_live_admin_source_form_submit($form, &$form_state) {
  //filter input for security
  $file = check_url($form_state['values']['bitgravity_live_default_file']);
  $bitrate = check_plain($form_state['values']['bitgravity_live_file_bit_rate']);
  $label = check_plain($form_state['values']['bitgravity_live_file_label']);
  db_query("INSERT INTO {bitgravity_live_sources} (file, bitrate, label) VALUES ('%s','%d','%s')", $file, $bitrate, $label);
  drupal_set_message(t('Added default source file %file.', array('%file' => $file)));
}